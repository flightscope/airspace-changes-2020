#!/bin/sh

set -e
set -x

script_dir=$(dirname "$0")
dist_dir=${script_dir}/../public
density_dist_dir=${dist_dir}/density
cropped_dist_dir=${dist_dir}/cropped
loop_dist_dir=${dist_dir}/loop
vtc_pdf_dir=${script_dir}/../pdf

mkdir -p ${dist_dir}
mkdir -p ${vtc_pdf_dir}

wget "https://www.airservicesaustralia.com/aip/pending/aipchart/vtc/Brisbane_Sunshine_VTC_21MAY2020.pdf" -nc -P ${vtc_pdf_dir}
wget "https://www.airservicesaustralia.com/aip/current/aipchart/vtc/Brisbane_Sunshine_VTC_07NOV2019.pdf" -nc -P ${vtc_pdf_dir}

pdf_files=$(cd ${vtc_pdf_dir} && find . -type f -name '*.pdf')

for pdf_file in $pdf_files; do
  basename=$(basename -- "${pdf_file}")
  filename="${basename%.*}"

  for density in 200 50; do
    mkdir -p ${density_dist_dir}/${density}
    if [ ! -f ${density_dist_dir}/${density}/${filename}.png ]; then
      convert -density ${density} -quality 100 ${vtc_pdf_dir}/${pdf_file} ${density_dist_dir}/${density}/${filename}.png
    else
      echo "file already exists: ${density_dist_dir}/${density}/${filename}.png"
    fi
  done
done

mkdir -p ${cropped_dist_dir}
for date in 07NOV2019 21MAY2020; do
  # YBSU
  if [ ! -f ${cropped_dist_dir}/ybsu_${date}.png ]; then
    convert -crop 2985x2380+800+2075 +repage ${density_dist_dir}/200/Brisbane_Sunshine_VTC_${date}.png ${cropped_dist_dir}/ybsu_${date}.png
  else
    echo "file already exists: ${cropped_dist_dir}/ybsu_${date}.png"
  fi

  # YBAF
  if [ ! -f ${cropped_dist_dir}/ybaf_${date}.png ]; then
    convert -crop 2590x3030+1200+4900 +repage ${density_dist_dir}/200/Brisbane_Sunshine_VTC_${date}.png ${cropped_dist_dir}/ybaf_${date}.png
  else
    echo "file already exists: ${cropped_dist_dir}/ybaf_${date}.png"
  fi
done

mkdir -p ${loop_dist_dir}
for delay in 300; do
  for icao in ybsu ybaf; do
    if [ ! -f ${loop_dist_dir}/${icao}_07NOV2019-21MAY2020.gif ]; then
      convert -delay ${delay} -loop 0 -dispose previous ${cropped_dist_dir}/${icao}_07NOV2019.png ${cropped_dist_dir}/${icao}_21MAY2020.png ${loop_dist_dir}/${icao}_07NOV2019-21MAY2020.gif
    else
      echo "file already exists: ${loop_dist_dir}/${icao}_07NOV2019-21MAY2020.gif"
    fi
  done
done
